# -*- coding: utf-8 -*-
import tornado.web
import tornado.ioloop
import json
from pymongo import MongoClient
import datetime
from memsql.common import database
import MySQLdb
from datetime import date, timedelta
from dateutil.parser import parse

#TWEETS Database
tweets_coll = MongoClient('104.199.191.34')["dataTagging"]["new_tweets"]   
tag_coll = MongoClient('104.199.191.34')["dataTagging"]["tweetsTagged"]
user_coll = MongoClient('104.199.191.34')["dataTagging"]["userInfo"]
stats_coll = MongoClient('104.199.191.34')["dataTagging"]["stats"]
total_tweets_count = tweets_coll.count()  
lim = 10

HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"

DATABASE = "datatagging"

def get_connection(db=DATABASE):
	return database.connect(host=HOST,user=USER, password=PASSWORD, database=db)

class TweetsTaggingHandler(tornado.web.RequestHandler):
	def get(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
		date_var = datetime.datetime.now().date()
		date_var = parse(str(date_var))
		uid = self.get_argument("uid",None)
		if uid is None:
			self.write(json.dumps({"Error Message":"Please provide uid"}))
		else:
			with get_connection(db=DATABASE) as conn:
				user_type = conn.query('SELECT user_type from users where uid=%s',uid)
				if user_type[0]["user_type"] == "employee":
					tweets_quota = 2000
				else:
					tweets_quota = 5000
			time_data = {}
			time_data["date"] = date_var
			time_data["count"] = 0
			query_user = user_coll.find_one({"uid":uid,"tweetsData":{"$exists":1}})
			if query_user is None:
				tweets_data = {}
				datewise_count = []
				datewise_count.append(time_data)
				query = user_coll.find_one({"uid":"admin"})
				last_visited = int(query["tweetsAllotedCount"])
				if last_visited >= total_tweets_count:
					self.set_header("Content-Type", "application/json")
					self.set_status(200)
					self.finish(json.dumps({"Error Message":"No more tweets data available"}))
					return
				start_row = int(query["tweetsAllotedCount"]+1)
				end_row = int(query["tweetsAllotedCount"]+tweets_quota)
				tweets_available_count = int(query["tweetsAvailableCount"]-tweets_quota)
				
				user_coll.update({"uid":"admin"},{"$set":{"tweetsAllotedCount":end_row,"tweetsAvailableCount":tweets_available_count}})
				
				tweets_data["lastVisited"] = last_visited
				tweets_data["tagCount"] = 0
				tweets_data["startRow"] = start_row
				tweets_data["endRow"] = end_row
				tweets_data["datewiseCount"] = datewise_count
				if user_coll.find({"uid":uid}).count() > 0:
					user_coll.update({"uid":uid},{"$set":{"tweetsData":tweets_data}})
				else:
					user_coll.insert({"uid":uid,"tweetsData":tweets_data})
			else:
				last_visited = query_user["tweetsData"]["lastVisited"]
				start_row = query_user["tweetsData"]["startRow"]
				end_row = query_user["tweetsData"]["endRow"]
				tag_count = query_user["tweetsData"]["tagCount"]
				datewise_count = query_user["tweetsData"]["datewiseCount"]
				if len(datewise_count) != 0:
					if user_coll.find({"uid":uid,"tweetsData.datewiseCount.date":date_var}).count() > 0:
						pass
					else:
						datewise_count.append(time_data)
						user_coll.update({"uid":uid},{"$set":{"tweetsData.datewiseCount":datewise_count}})
				if tag_count >= 50000:
					self.finish(json.dumps({"Error Message":"You have exceeded your quota of 50000 tweets. Please Contact: support@paralleldots.com"}))
					return
				if last_visited >= end_row:
					last_visited = start_row-1
			cursor = tweets_coll.find({"tagged":0,"tweetsId":{"$gt":last_visited,"$lte":end_row}}).limit(lim)
			tweets_count = cursor.count(True)
			tweets_list = []
			data = {} 
			count = 0
			for cur in cursor:
				tweets = cur["text"]
				tweets_list.append(tweets)
				count+=1
				tweets_id = cur["tweetsId"]
				if tweets_count < lim:
					user_coll.update({"uid":uid},{"$set":{"tweetsData.lastVisited":end_row}})
				elif tweets_count == count:
					user_coll.update({"uid":uid},{"$set":{"tweetsData.lastVisited":tweets_id}})
			data["data"] = tweets_list
			data["next"] = "http://utilities.paralleldots.com/tweetstagging?uid=%s"%(uid)
			self.write(data)

	def post(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')

		tweets_data = json.loads(self.request.body)
		#print news_data
		now = datetime.datetime.now()
		date_var = datetime.datetime.now().date()
		date_var = parse(str(date_var))
		query_stats = stats_coll.find_one({"date":date_var,"tweetsStats":{"$exists":1}})
		if query_stats is not None:
			sentiment_stats = query_stats["tweetsStats"]["sentiment"]
			emotion_stats = query_stats["tweetsStats"]["emotion"]
			classifier_stats = query_stats["tweetsStats"]["classifier"]
			adult_stats = query_stats["tweetsStats"]["adult"]
			abusive_stats = query_stats["tweetsStats"]["abusive"]
		else:
			sentiment_stats = {"Positive":0,"Negative":0,"Neutral":0}
			emotion_stats = {"happy":0,"excited":0,"angry":0,"sad":0,"bored":0,"disgusted":0,"funny":0,"fear":0,"indifferent":0}
			classifier_stats = {"news":0,"feedback":0,"Feedback":{"appreciation":0,"spam/junk":0,"complaint":0,"suggestion":0},"opinion":0,"marketing":0,"troll":0,"random":0,"spam/junk":0,"query":0}
			adult_stats = 0
			abusive_stats = 0
		count = 0
		for tweets in tweets_data:
			count+=1
			user_id = tweets.get('uid',None)
			tweet = tweets.get('heading',None)
			if tweet is not None:
				tweets_coll.update({"text":tweet},{"$set":{"tagged":1}})
			query = user_coll.find_one({"uid":user_id})
			if query is not None:
				sentiment = tweets.get("sentiment",None)
				if sentiment is not None:
					sentiment_stats[sentiment]+=1
				emotion = tweets.get("emotion",None)
				if emotion is not None:
					emotion_stats[emotion]+=1
				classifier = tweets.get("classifier",None)
				if classifier is not None:
					if len(classifier) == 1:
						feedback = classifier.values()[0]
						if len(feedback) == 1:
							feed = feedback.keys()[0]
							feed = feed.lower()
							classifier_stats["Feedback"][feed]+=1
						else:
							feedback = feedback.lower()
							classifier_stats["Feedback"][feedback]+=1
					else:
						classifier_stats[classifier]+=1
				adult = tweets.get("Adult",None)
				if adult is True:
					adult_stats += 1
				abusive = tweets.get("Abusive",None)
				if abusive is True:
					abusive_stats += 1
				tag_coll.insert({"tweet":tweet,"sentiment":sentiment,"classifier":classifier,"emotion":emotion,"uid":user_id,"timestamp":now,"date":date_var,"adult":adult,"abusive":abusive})
			else:
				print "User not present"

		# datewise_count = []
		temp_count = query["tweetsData"]["datewiseCount"]
		tag_count = query['tweetsData']['tagCount']
		print "tag_count",tag_count
		for temp in temp_count:
		 	if temp["date"] == date_var:
		 		c = temp["count"]
		 		final_count = c+count
		 		user_coll.update({"$and":[{"uid":user_id},{"tweetsData.datewiseCount.date":date_var}]},{"$set":{"tweetsData.datewiseCount.$.count":final_count,"tweetsData.tagCount":tag_count+count}})
		 	else:
		 		user_coll.update({"$and":[{"uid":user_id},{"tweetsData.datewiseCount.date":date_var}]},{"$set":{"tweetsData.datewiseCount.$.count":count,"tweetsData.tagCount":tag_count+count}})

		tweets_stats = {"sentiment":sentiment_stats,"emotion":emotion_stats,"classifier":classifier_stats,"adult":adult_stats,"abusive":abusive_stats}
		query_stats = stats_coll.find_one({"date":date_var})
		if query_stats is None:
			stats_coll.insert({"date":date_var,"tweetsStats":tweets_stats})
		else:
			stats_coll.update({"date":date_var},{"$set":{"tweetsStats":tweets_stats}})

class UserInfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        data = {}
        d = {}
        uid = self.get_argument("uid")
        query = user_coll.find_one({'uid':uid})
        if query is not None:
            d = {}
            d['uid'] = query['uid']
            d['tag_count'] = query["tweetsData"]["tagCount"]
        else:
        	self.write(json.dumps({"Error Message":"User not present"}))

        data['info'] = d
        self.write(data)

def make_app():
	return tornado.web.Application([
		(r"/tweetstagging",TweetsTaggingHandler),(r"/tweetstagging/userinfo",UserInfoHandler)
	])

if __name__ == "__main__":
	app = make_app()
	app.listen(8881)
	tornado.ioloop.IOLoop.current().start()
