# -*- coding: utf-8 -*-
from pymongo import MongoClient

old_news = MongoClient('104.155.210.134')["tagging"]["headlines"]
news = MongoClient('104.155.210.134')["dataTagging"]["news"]

cursor = old_news.find({"flag":0}).sort("_id",-1).limit(150000)
news_id = 0
for cur in cursor:
	heading = cur["heading"]
	news_id += 1
	news.insert({"newsHeadline":heading,"newsId":news_id,"tagged":0})
	