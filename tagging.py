# -*- coding: utf-8 -*-
import tornado.web
import tornado.ioloop
import json
from pymongo import MongoClient
import datetime
from memsql.common import database
import MySQLdb
from datetime import date, timedelta
from dateutil.parser import parse

#NEWS Database
news_coll = MongoClient('104.155.210.134')["dataTagging"]["news"]   
tag_coll = MongoClient('104.155.210.134')["dataTagging"]["newsTagged"]
user_coll = MongoClient('104.155.210.134')["dataTagging"]["userInfo"]
stats_coll = MongoClient('104.155.210.134')["dataTagging"]["stats"]
total_news_count = news_coll.count()  
lim = 10

HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"

DATABASE = "datatagging"

date = datetime.datetime.now().date()
date = parse(str(date))

def get_connection(db=DATABASE):
	return database.connect(host=HOST,user=USER, password=PASSWORD, database=db)

class NewsTaggingHandler(tornado.web.RequestHandler):
	def get(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')

		uid = self.get_argument("uid",None)
		if uid is None:
			self.write(json.dumps({"Error Message":"Please provide uid"}))
		else:
			with get_connection(db=DATABASE) as conn:
				user_type = conn.query('SELECT user_type from users where uid=%s',uid)
				if user_type[0]["user_type"] == "employee":
					news_quota = 2000
				else:
					news_quota = 5000
			time_data = {}
			time_data["date"] = date
			time_data["count"] = 0
			query_user = user_coll.find_one({"uid":uid,"newsData":{"$exists":1}})
			if query_user is None:
				print "user not present"
				news_data = {}
				datewise_count = []
				datewise_count.append(time_data)
				query = user_coll.find_one({"uid":"admin"})
				last_visited = int(query["newsAllotedCount"])
				if last_visited >= total_news_count:
					self.set_header("Content-Type", "application/json")
					self.set_status(200)
					self.finish(json.dumps({"Error Message":"No more news data available"}))
					return
				start_row = int(query["newsAllotedCount"]+1)
				end_row = int(query["newsAllotedCount"]+news_quota)
				news_available_count = int(query["newsAvailableCount"]-news_quota)
				
				user_coll.update({"uid":"admin"},{"$set":{"newsAllotedCount":end_row,"newsAvailableCount":news_available_count}})
				
				news_data["lastVisited"] = last_visited
				news_data["tagCount"] = 0
				news_data["startRow"] = start_row
				news_data["endRow"] = end_row
				news_data["datewiseCount"] = datewise_count
				if user_coll.find({"uid":uid}).count() > 0:
					user_coll.update({"uid":uid},{"$set":{"newsData":news_data}})
				else:
					user_coll.insert({"uid":uid,"newsData":news_data})
			else:
				print "user present"
				last_visited = query_user["newsData"]["lastVisited"]
				start_row = query_user["newsData"]["startRow"]
				end_row = query_user["newsData"]["endRow"]
				tag_count = query_user["newsData"]["tagCount"]
				datewise_count = query_user["newsData"]["datewiseCount"]
				if len(datewise_count) != 0:
					if user_coll.find({"uid":uid,"newsData.datewiseCount.date":date}).count() > 0:
						pass
					else:
						datewise_count.append(time_data)
						user_coll.update({"uid":uid},{"$set":{"newsData.datewiseCount":datewise_count}})
				if tag_count == news_quota:
					self.finish(json.dumps({"Error Message":"You have exceeded your quota of 25000 news. Please Contact: support@paralleldots.com"}))
					return
				if last_visited >= end_row:
					last_visited = start_row-1
			cursor = news_coll.find({"tagged":0,"newsId":{"$gt":last_visited,"$lte":end_row}}).limit(lim)
			news_count = cursor.count(True)
			news_list = []
			data = {} 
			count = 0
			for cur in cursor:
				news = cur["newsHeadline"]
				news_list.append(news)
				count+=1
				news_id = cur["newsId"]
				if news_count < lim:
					user_coll.update({"uid":uid},{"$set":{"newsData.lastVisited":end_row}})
				elif news_count == count:
					user_coll.update({"uid":uid},{"$set":{"newsData.lastVisited":news_id}})
			data["data"] = news_list
			data["next"] = "http://utilities.paralleldots.com/tagging?uid=%s"%(uid)
			self.write(data)

	def post(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')

		news_data = json.loads(self.request.body)
		now = datetime.datetime.now()
		date = datetime.datetime.now().date()
		date = parse(str(date))
		query_stats = stats_coll.find_one({"date":date,"newsStats":{"$exists":1}})
		if query_stats is not None:
			sentiment_stats = query_stats["newsStats"]["sentiment"]
			emotion_stats = query_stats["newsStats"]["emotion"]
			classifier_stats = query_stats["newsStats"]["classifier"]
			adult_stats = query_stats["newsStats"]["adult"]
			abusive_stats = query_stats["newsStats"]["abusive"]
		else:
			sentiment_stats = {"Positive":0,"Negative":0,"Neutral":0}
			emotion_stats = {"happy":0,"excited":0,"angry":0,"sad":0,"bored":0,"disgusted":0,"funny":0,"fear":0,"indifferent":0}
			classifier_stats = {"news":0,"feedback":0,"Feedback":{"appreciation":0,"spam/junk":0,"complaint":0,"suggestion":0},"opinion":0,"marketing":0,"troll":0,"random":0,"spam/junk":0}
			adult_stats = 0
			abusive_stats = 0
		count = 0
		for news in news_data:
			count+=1
			user_id = news.get('uid',None)
			news_headline = news.get('heading',None)
			if news_headline is not None:
				news_coll.update({"newsHeadline":news_headline},{"$set":{"tagged":1}})
			query = user_coll.find_one({"uid":user_id})
			if query is not None:
				sentiment = news.get("sentiment",None)
				if sentiment is not None:
					sentiment_stats[sentiment]+=1
				emotion = news.get("emotion",None)
				if emotion is not None:
					emotion_stats[emotion]+=1
				classifier = news.get("classifier",None)
				if classifier is not None:
					if len(classifier) == 1:
						feedback = classifier.values()[0]
						if len(feedback) == 1:
							feed = feedback.keys()[0]
							feed = feed.lower()
							classifier_stats["Feedback"][feed]+=1
						else:
							feedback = feedback.lower()
							classifier_stats["Feedback"][feedback]+=1
					else:
						classifier_stats[classifier]+=1
				adult = news.get("Adult",None)
				if adult is True:
					adult_stats += 1
				abusive = news.get("Abusive",None)
				if abusive is True:
					abusive_stats+=1
				tag_coll.insert({"newsHeadline":news_headline,"sentiment":sentiment,"classifier":classifier,"emotion":emotion,"uid":user_id,"timestamp":now,"date":date,"adult":adult,"abusive":abusive})
			else:
				print "User not present"

		temp_count = query["newsData"]["datewiseCount"]
		tag_count = query['newsData']['tagCount']
		for temp in temp_count:
		 	if temp["date"] == date:
		 		c = temp["count"]
		 		final_count = c+count
		 		user_coll.update({"$and":[{"uid":user_id},{"newsData.datewiseCount.date":date}]},{"$set":{"newsData.datewiseCount.$.count":final_count,"newsData.tagCount":tag_count+count}})
		 	else:
		 		user_coll.update({"$and":[{"uid":user_id},{"newsData.datewiseCount.date":date}]},{"$set":{"newsData.datewiseCount.$.count":count,"newsData.tagCount":tag_count+count}})

		news_stats = {"sentiment":sentiment_stats,"emotion":emotion_stats,"classifier":classifier_stats,"adult":adult_stats,"abusive":abusive_stats}
		query_stats = stats_coll.find_one({"date":date})
		if query_stats is None:
			stats_coll.insert({"date":date,"newsStats":news_stats})
		else:
			stats_coll.update({"date":date},{"$set":{"newsStats":news_stats}})

class UserInfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        data = {}
        d = {}
        uid = self.get_argument("uid")
        query = user_coll.find_one({'uid':uid})
        if query is not None:
            d = {}
            d['uid'] = query['uid']
            d['tag_count'] = query["newsData"]["tagCount"]
        else:
        	self.write(json.dumps({"Error Message":"User not present"}))

        data['info'] = d
        self.write(data)

def make_app():
	return tornado.web.Application([
		(r"/tagging",NewsTaggingHandler),(r"/userinfo",UserInfoHandler)
	])

if __name__ == "__main__":
	app = make_app()
	app.listen(8886)
	tornado.ioloop.IOLoop.current().start()
